## Enterprise Application
_by Karol Telus_

This is the skeleton of multi-tiered enterprise application that has been written basing on the JAVA 8 and Spring framework. The application stores data about the premises, their owners and tenants.

The functionality of the application is separated into three isolated functional areas:

- The layer for handling http requests (controllers)
- Business logic layer implemented using Spring services (services)
- Data persistence layer 

The following Spring modules are used:

- Spring MVC
- Spring Core 
- Spring Data
