package com.telus.service;

import com.telus.converter.UserConverter;
import com.telus.dto.User;
import com.telus.model.CustomerEntity;
import com.telus.model.UserEntity;

import java.util.function.BiFunction;

class UserUpdater implements BiFunction<UserEntity, User, UserEntity> {

    private final UserConverter userConverter;

    public UserUpdater(UserConverter userConverter) {
        this.userConverter = userConverter;
    }

    @Override
    public UserEntity apply(UserEntity entity, User dto) {
        UserEntity tmp = userConverter.fromDto(dto);
        entity.setName(tmp.getName());
        entity.setProfile(CustomerEntity.Profile.valueOf(tmp.getProfile().name()));
        return entity;
    }
}
