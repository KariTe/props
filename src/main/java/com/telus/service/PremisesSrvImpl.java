package com.telus.service;

import com.telus.converter.AddressConverter;
import com.telus.converter.PremisesConverter;
import com.telus.dto.Premises;
import com.telus.model.PremisesEntity;
import com.telus.repository.PremisesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PremisesSrvImpl implements PremisesSrv {

    @Autowired
    PremisesConverter premisesConverter;

    @Autowired
    AddressConverter addressConverter;

    @Autowired
    PremisesRepository premisesRepository;


    @Override
    public long create(Premises dto) {
        PremisesEntity premisesEntity = premisesConverter.fromDto(dto);
        premisesRepository.save(premisesEntity);
        return premisesEntity.getId();
    }

    @Override
    public Premises find(Long id) throws EntityNotFoundException {
        PremisesEntity premisesEntity = premisesRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return premisesConverter.fromEntity(premisesEntity);
    }

    @Override
    public List<Premises> findAll() {
        return premisesRepository.findAll().stream()
                .map(p-> premisesConverter.fromEntity(p))
                .collect(Collectors.toList());
    }

    @Override
    public long update(Premises dto) {
       return premisesRepository.findById(dto.getId())
                .map(premisesEntity -> new PremisesUpdater(premisesConverter).apply(premisesEntity, dto))
                .map(premisesEntity -> new PremisesAddressUpdater(addressConverter).apply(premisesEntity, dto))
                .map(premisesEntity -> premisesRepository.save(premisesEntity))
                .map(PremisesEntity::getId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void delete(Long id) {
        premisesRepository.deleteById(id);
    }
}
