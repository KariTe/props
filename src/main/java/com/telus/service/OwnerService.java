package com.telus.service;
import com.telus.dto.Owner;
import com.telus.dto.Premises;

import java.util.List;
import java.util.Set;

public interface OwnerService {

    long create (Owner dto);
    Owner find (Long id);
    List<Owner> findAll();
    long update (Owner dto);
    void delete (Long id);
    long addPremises (Long ownerId, Premises dto);
    long updatePremises(Long ownerId, Long premisesId, Premises premisesDto);
    Set<Premises> findPremisesByOwnerId (Long ownerId);
    void deleteOnePremises(Long ownerId, Long premisesId);
    Premises getOnePremises(Long ownerId, Long premisesId);

}
