package com.telus.service;

import com.telus.converter.OwnerConverter;
import com.telus.dto.Owner;
import com.telus.model.CustomerEntity;
import com.telus.model.OwnerEntity;

import java.util.function.BiFunction;

public class OwnerUpdater implements BiFunction <OwnerEntity, Owner, OwnerEntity> {

    OwnerConverter ownerConverter;

    public OwnerUpdater(OwnerConverter ownerConverter) {
        this.ownerConverter = ownerConverter;
    }

    @Override
    public OwnerEntity apply(OwnerEntity ownerEntity, Owner dto) {
        OwnerEntity tmp = ownerConverter.fromDto(dto);
        ownerEntity.setName(tmp.getName());
        ownerEntity.setProfile(CustomerEntity.Profile.valueOf(tmp.getProfile().name()));
        ownerEntity.setPremises(tmp.getPremises());
        return ownerEntity;
    }
}
