package com.telus.service;

import com.telus.converter.UserConverter;
import com.telus.dto.User;
import com.telus.model.CustomerEntity;
import com.telus.model.UserEntity;
import com.telus.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class UserSrvImp implements UserSrv {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserConverter userConverter;

    @Override
    public long create(User userDto) {
        UserEntity userEntity = userConverter.fromDto(userDto);
        userRepository.saveAndFlush(userEntity);
        return userEntity.getId();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User find(Long id) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return userConverter.fromEntity(userEntity);
    }

    @Override
    public List<User> findAll() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(obj -> userConverter.fromEntity(obj))
                .collect(Collectors.toList());
    }

    @Override
    public long update(User userDto) {
        return userRepository.findById(userDto.getId())
                .map(entity -> new UserUpdater(userConverter).apply(entity, userDto))
                .map(entity -> userRepository.save(entity))
                .map(CustomerEntity::getId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

}
