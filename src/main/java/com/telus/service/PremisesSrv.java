package com.telus.service;

import com.telus.dto.Premises;

import java.util.List;

public interface PremisesSrv {

    long create (Premises dto);
    Premises find (Long id);
    List<Premises> findAll();
    long update(Premises dto);
    void delete(Long id);

}
