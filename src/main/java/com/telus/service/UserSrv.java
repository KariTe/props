package com.telus.service;
import com.telus.dto.User;

import java.util.List;


public interface UserSrv {

    long create(User dto);
    User find(Long id);
    List<User> findAll();
    long update(User dto);
    void delete(Long id);

}
