package com.telus.service;

import com.telus.converter.PremisesConverter;
import com.telus.dto.Premises;
import com.telus.model.PremisesEntity;

import java.util.function.BiFunction;

public class PremisesUpdater implements BiFunction <PremisesEntity, Premises, PremisesEntity> {

   private final PremisesConverter premisesConverter;

    public PremisesUpdater(PremisesConverter premisesConverter) {
        this.premisesConverter = premisesConverter;
    }

    @Override
    public PremisesEntity apply(PremisesEntity entity, Premises dto) {
        PremisesEntity tmp = premisesConverter.fromDto(dto);
        entity.setParkingSpace(tmp.getParkingSpace());
        entity.setFloor(tmp.getFloor());
        entity.setCustomerEntity(tmp.getCustomerEntity());
        entity.setArea(tmp.getArea());
        entity.setSqmPrice(tmp.getSqmPrice());
        entity.setAddsWindow(tmp.getAddsWindow());
        return entity;
    }

}
