package com.telus.service;

import com.telus.converter.AddressConverter;
import com.telus.dto.Premises;
import com.telus.model.AddressEntity;
import com.telus.model.PremisesEntity;

import java.util.Optional;
import java.util.function.BiFunction;

public class PremisesAddressUpdater implements BiFunction<PremisesEntity, Premises, PremisesEntity> {

    private final AddressConverter addressConverter;

    public PremisesAddressUpdater(AddressConverter addressConverter) {
        this.addressConverter = addressConverter;
    }

    @Override
    public PremisesEntity apply(PremisesEntity entity, Premises premises) {
        AddressEntity tmp = addressConverter.fromDto(premises.getAddress());

        Optional.ofNullable(entity)
                .map(PremisesEntity::getAddress)
                .ifPresent(a -> {
                    a.setCity(tmp.getCity());
                    a.setPostCode(tmp.getPostCode());
                    a.setStreet(tmp.getStreet());
                    a.setStreetNumber(tmp.getStreetNumber());
                });
        return entity;
    }
}
