package com.telus.service;

import com.telus.converter.AddressConverter;
import com.telus.converter.OwnerConverter;
import com.telus.converter.PremisesConverter;
import com.telus.dto.Owner;
import com.telus.dto.Premises;
import com.telus.model.AddressEntity;
import com.telus.model.CustomerEntity;
import com.telus.model.OwnerEntity;
import com.telus.model.PremisesEntity;
import com.telus.repository.OwnerRepository;
import com.telus.repository.PremisesAddressRepository;
import com.telus.repository.PremisesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    PremisesRepository premisesRepository;

    @Autowired
    OwnerConverter ownerConverter;

    @Autowired
    PremisesConverter premisesConverter;

    @Autowired
    AddressConverter addressConverter;

    @Override
    public long create(Owner ownerDto) {
        OwnerEntity ownerEntity = ownerConverter.fromDto(ownerDto);
        ownerRepository.save(ownerEntity);
        return ownerEntity.getId();
    }

    @Override
    public Owner find(Long id) throws EntityNotFoundException{
        OwnerEntity ownerEntity = ownerRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return ownerConverter.fromEntity(ownerEntity);
    }

    @Override
    public List<Owner> findAll() {
        return ownerRepository.findAll().stream()
                .map(o -> ownerConverter.fromEntity(o))
                .collect(Collectors.toList());
    }

    @Override
    public long update(Owner dto) {
        return ownerRepository.findById(dto.getId())
                .map(ownerEntity -> new OwnerUpdater(ownerConverter).apply(ownerEntity, dto))
                .map(ownerEntity -> ownerRepository.save(ownerEntity))
                .map(CustomerEntity::getId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void delete(Long id) {
        ownerRepository.deleteById(id);
    }

    @Override
    public long addPremises(Long ownerId, Premises dto) {
        OwnerEntity owner = ownerRepository.findById(ownerId).orElseThrow(NoSuchElementException::new);
        PremisesEntity premisesEntity = premisesConverter.fromDto(dto);
        premisesEntity.setOwner(owner);
        premisesRepository.save(premisesEntity);
        return premisesEntity.getId();
    }

    @Override
    public long updatePremises(Long ownerId, Long premisesId, Premises dto)  throws NoSuchElementException {
        ownerRepository.findById(ownerId).orElseThrow(NoSuchElementException::new);
        return premisesRepository.findById(premisesId)
                .map(premisesEntity -> new PremisesUpdater(premisesConverter).apply(premisesEntity, dto))
                .map(premisesEntity -> new PremisesAddressUpdater(addressConverter).apply(premisesEntity, dto))
                .map(premisesEntity -> premisesRepository.save(premisesEntity))
                .map(PremisesEntity::getId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Set<Premises> findPremisesByOwnerId(Long ownerId) {
        return Optional.ofNullable(ownerRepository.findOwnerByIdWithPremises(ownerId))
                .orElseThrow(NoSuchElementException::new)
                .getPremises()
                .stream()
                .map(e -> premisesConverter.fromEntity(e))
                .collect(Collectors.toSet());
    }

    @Override
    public void deleteOnePremises(Long ownerId, Long premisesId) throws NoSuchElementException {
        ownerRepository.findById(ownerId).orElseThrow(NoSuchElementException::new);
        premisesRepository.deleteOnePremises(premisesId);
    }

    @Override
    public Premises getOnePremises(Long ownerId, Long premisesId) throws NoSuchElementException {
        ownerRepository.findById(ownerId).orElseThrow(NoSuchElementException::new);
        return premisesConverter.fromEntity(premisesRepository.getOnePremises(premisesId));
    }

}
