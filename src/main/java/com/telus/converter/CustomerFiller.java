package com.telus.converter;

import com.telus.dto.Customer;
import com.telus.dto.Customerable;
import com.telus.model.CustomerEntity;
import org.springframework.stereotype.Component;

@Component
public class CustomerFiller {

    public void fillEntity(Customerable customerable, CustomerEntity entity) {
        entity.setId(customerable.getId());
        entity.setName(customerable.getName());
        entity.setProfile(from(customerable.getProfile()));
    }

    public void fillDto(CustomerEntity entity, Customerable customerable) {
        customerable.setId(entity.getId());
        customerable.setName(entity.getName());
        customerable.setProfile(from(entity.getProfile()));
    }

    public CustomerEntity.Profile from(Customer.Profile profile) throws IllegalArgumentException {
        switch (profile){
            case PRIVATE: return CustomerEntity.Profile.PRIVATE;
            case INSTITUTIONAL: return CustomerEntity.Profile.INSTITUTIONAL;
            default:throw new IllegalArgumentException();
        }
    }

    public Customer.Profile from(CustomerEntity.Profile profile) throws IllegalArgumentException {
        switch(profile){
            case PRIVATE: return  Customer.Profile.PRIVATE;
            case INSTITUTIONAL: return Customer.Profile.INSTITUTIONAL;
            default:throw new IllegalArgumentException();
        }
    }

}
