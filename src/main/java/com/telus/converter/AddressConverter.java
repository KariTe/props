package com.telus.converter;

import com.telus.dto.PremisesAddress;
import com.telus.model.AddressEntity;
import org.springframework.stereotype.Component;

@Component
public class AddressConverter implements Converter<AddressEntity, PremisesAddress> {
    @Override
    public AddressEntity fromDto(PremisesAddress dto) {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setCity(dto.getCity());
        addressEntity.setPostCode(dto.getPostCode());
        addressEntity.setStreet(dto.getStreet());
        addressEntity.setStreetNumber(dto.getStreetNumber());
        return addressEntity;
    }

    @Override
    public PremisesAddress fromEntity(AddressEntity entity) {
        PremisesAddress premisesAddress = new PremisesAddress();
        premisesAddress.setCity(entity.getCity());
        premisesAddress.setPostCode(entity.getPostCode());
        premisesAddress.setStreet(entity.getStreet());
        premisesAddress.setStreetNumber(entity.getStreetNumber());
        premisesAddress.setPostCode(entity.getPostCode());
        return  premisesAddress;
    }
}
