package com.telus.converter;

import com.telus.dto.Premises;
import com.telus.model.PremisesEntity;
import org.springframework.stereotype.Component;

@Component
public class PremisesConverter implements Converter<PremisesEntity, Premises>{

    AddressConverter addressConverter = new AddressConverter();

    @Override
    public PremisesEntity fromDto(Premises dto) {
        PremisesEntity premisesEntity = new PremisesEntity();
        premisesEntity.setArea(dto.getArea());
        premisesEntity.setAddsWindow(dto.getAddsWindow());
        premisesEntity.setParkingSpace(dto.getParkingSpace());
        premisesEntity.setFloor(dto.getFloor());
        premisesEntity.setSqmPrice(dto.getSqmPrice());
        premisesEntity.setAddress(addressConverter.fromDto(dto.getAddress()));
        return premisesEntity;
    }

    @Override
    public Premises fromEntity(PremisesEntity entity) {
        Premises dto = new Premises();
        dto.setId(entity.getId());
        dto.setArea(entity.getArea());
        dto.setAddsWindow(entity.getAddsWindow());
        dto.setParkingSpace(entity.getParkingSpace());
        dto.setFloor(entity.getFloor());
        dto.setSqmPrice(entity.getSqmPrice());
        dto.setAddress(addressConverter.fromEntity(entity.getAddress()));
        dto.setOwnerId(entity.getOwner().getId());
        return dto;
    }

}
