package com.telus.converter;

import com.telus.dto.User;
import com.telus.model.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<UserEntity, User> {

    private static final CustomerFiller CUSTOMER_FILLER = new CustomerFiller();

    @Override
    public UserEntity fromDto(User dto) {
        UserEntity entity = new UserEntity();
        CUSTOMER_FILLER.fillEntity(dto, entity);
        entity.setUserBusinessType(from(dto.getUserBusinessType()));
        return entity;
    }

    @Override
    public User fromEntity(UserEntity entity) {
        User user = new User();
        CUSTOMER_FILLER.fillDto(entity, user);
        user.setUserBusinessType(from(entity.getUserBusinessType()));
        return user;
    }

    public UserEntity.UserBusinessType from(User.UserBusinessType type) throws IllegalArgumentException {
        switch (type) {
            case OTHER:
                return UserEntity.UserBusinessType.OTHER;
            case SALE:
                return UserEntity.UserBusinessType.SALE;
            case SERVICES:
                return UserEntity.UserBusinessType.SERVICES;
            default:
                throw new IllegalArgumentException();
        }
    }

    public User.UserBusinessType from(UserEntity.UserBusinessType type) throws IllegalArgumentException {
        switch (type) {
            case OTHER:
                return  User.UserBusinessType.OTHER;
            case SALE:
                return  User.UserBusinessType.SALE;
            case SERVICES:
                return  User.UserBusinessType.SERVICES;
            default:
                throw  new IllegalArgumentException();
        }
    }

}