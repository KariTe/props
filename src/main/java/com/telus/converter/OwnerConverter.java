package com.telus.converter;

import com.telus.dto.Owner;
import com.telus.model.OwnerEntity;
import org.springframework.stereotype.Component;

@Component
public class OwnerConverter implements Converter<OwnerEntity, Owner>{

    CustomerFiller customerFiller = new CustomerFiller();

    @Override
    public OwnerEntity fromDto(Owner dto) {
       OwnerEntity ownerEntity = new OwnerEntity();
        customerFiller.fillEntity(dto, ownerEntity);
        return ownerEntity;
    }

    @Override
    public Owner fromEntity(OwnerEntity entity) {
        Owner owner = new Owner();
        customerFiller.fillDto(entity, owner);
        return owner;
    }

}
