package com.telus.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@DiscriminatorValue("OWNERS")
public class OwnerEntity extends CustomerEntity {

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<PremisesEntity> premises;

    public OwnerEntity() {
    }

    public OwnerEntity(List<PremisesEntity> premises) {
        this.premises = premises;
    }

    public List<PremisesEntity> getPremises() {
        return premises;
    }

    public void setPremises(List<PremisesEntity> premises) {
        this.premises = premises;
    }
}
