package com.telus.model;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("USERS")
public class UserEntity extends CustomerEntity {

    public enum UserBusinessType {
        SERVICES, SALE, OTHER
    }

    @Column(name = "BUSINESS_TYPE")
    @Enumerated(EnumType.STRING)
    private UserBusinessType userBusinessType;

    @OneToMany(mappedBy = "customerEntity", fetch = FetchType.LAZY)
    private List<PremisesEntity> premises;

    public UserEntity() {
    }

    public UserEntity(Long id, String name, Profile profile, List<PremisesEntity> premises, UserBusinessType userBusinessType) {
        super(id, name, profile);
        this.premises = premises;
        this.userBusinessType = userBusinessType;
    }

    public UserBusinessType getUserBusinessType() {
        return userBusinessType;
    }

    public void setUserBusinessType(UserBusinessType userBusinessType) {
        this.userBusinessType = userBusinessType;
    }

    public List<PremisesEntity> getPremises() {
        return premises;
    }

    public void setPremises(List<PremisesEntity> premises) {
        this.premises = premises;
    }
}
