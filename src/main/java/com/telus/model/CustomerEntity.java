package com.telus.model;

import javax.persistence.*;

@Entity
@Table(name = "CUSTOMERS_TABLE")
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "TYPE")
public abstract class CustomerEntity {

    public enum Profile {
        INSTITUTIONAL, PRIVATE
    }

    @Id
    @GeneratedValue
    @Column(name = "CUSTOMER_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PROFILE")
    @Enumerated(EnumType.STRING)
    private Profile profile;

    public CustomerEntity(Long id, String name, Profile profile) {
        this.id = id;
        this.name = name;
        this.profile = profile;
    }

    public CustomerEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
