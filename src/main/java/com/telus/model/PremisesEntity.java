package com.telus.model;

import javax.persistence.*;

@Entity
@Table(name="PREMISES_TABLE")
public class PremisesEntity{

    @Id
    @GeneratedValue
    @Column(name = "PREMISES_ID")
    private Long id;

    @Column(name = "FLOOR")
    private String floor;

    @Column(name = "ADDS_WINDOW")
    private boolean addsWindow;

    @Column(name = "PARKING_SPACE")
    private Boolean parkingSpace;

    @Column(name = "AREA")
    private Integer area;

    @Column(name = "SQM_PRICE")
    private Integer sqmPrice;

    @JoinColumn(name = "OWNER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerEntity owner;

    @JoinColumn(name = "CUSTOMER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerEntity customerEntity;

    @OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID")
    private AddressEntity address;

    public PremisesEntity(Long id, String floor, boolean addsWindow, Boolean parkingSpace, Integer area, Integer sqmPrice, CustomerEntity owner, CustomerEntity customerEntity, AddressEntity address) {
        this.id = id;
        this.floor = floor;
        this.addsWindow = addsWindow;
        this.parkingSpace = parkingSpace;
        this.area = area;
        this.sqmPrice = sqmPrice;
        this.owner = owner;
        this.customerEntity = customerEntity;
        this.address = address;
    }

    public PremisesEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public boolean getAddsWindow() {
        return addsWindow;
    }

    public void setAddsWindow(boolean addsWindow) {
        this.addsWindow = addsWindow;
    }

    public Boolean getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(Boolean parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getSqmPrice() {
        return sqmPrice;
    }

    public void setSqmPrice(Integer sqmPrice) {
        this.sqmPrice = sqmPrice;
    }

    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public CustomerEntity getOwner() {
        return owner;
    }

    public void setOwner(CustomerEntity owner) {
        this.owner = owner;
    }
}
