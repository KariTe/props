package com.telus.dto;

public class Premises {

    private Long id;

    private Long ownerId;

    private PremisesAddress address;

    private String floor;

    private boolean addsWindow;

    private boolean parkingSpace;

    private int area;

    private int sqmPrice;

    public Premises(Long id, Long ownerId, PremisesAddress address, String floor, boolean addsWindow,
                    boolean parkingSpace, int area, int sqmPrice) {
        this.id = id;
        this.ownerId = ownerId;
        this.address = address;
        this.floor = floor;
        this.addsWindow = addsWindow;
        this.parkingSpace = parkingSpace;
        this.area = area;
        this.sqmPrice = sqmPrice;
    }

    public Premises() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PremisesAddress getAddress() {
        return address;
    }

    public void setAddress(PremisesAddress address) {
        this.address = address;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public boolean getAddsWindow() {
        return addsWindow;
    }

    public void setAddsWindow(boolean addsWindow) {
        this.addsWindow = addsWindow;
    }

    public boolean getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(boolean parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getSqmPrice() {
        return sqmPrice;
    }

    public void setSqmPrice(int sqmPrice) {
        this.sqmPrice = sqmPrice;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
}
