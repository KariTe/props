package com.telus.dto;

public class Owner implements Customerable{

    private Long id;

    private String name;

    private Customer.Profile profile;

    public Owner(Long id, String name, Customer.Profile profile) {
        this.id = id;
        this.name = name;
        this.profile = profile;
    }

    public Owner() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Customer.Profile getProfile() {
        return profile;
    }

    @Override
    public void setProfile(Customer.Profile profile) {
        this.profile = profile;
    }
}
