package com.telus.dto;

import com.telus.dto.Customer.Profile;

public class User implements Customerable{


    public enum UserBusinessType {
        SERVICES, SALE, OTHER
    }

    private Long id;

    private String name;

    private Profile profile;

    private User.UserBusinessType userBusinessType;

    public User(Long id, String name, Profile profile, User.UserBusinessType type) {
        this.id = id;
        this.name = name;
        this.profile = profile;
        this.userBusinessType = type;
    }

    public User() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Profile getProfile() {
        return profile;
    }

    @Override
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public UserBusinessType getUserBusinessType() {
        return userBusinessType;
    }

    public void setUserBusinessType(UserBusinessType userBusinessType) {
        this.userBusinessType = userBusinessType;
    }

}
