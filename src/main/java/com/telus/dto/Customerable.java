package com.telus.dto;

public interface Customerable {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    Customer.Profile getProfile();

    void setProfile(Customer.Profile profile);

}
