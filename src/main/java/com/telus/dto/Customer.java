package com.telus.dto;

public class Customer {

    public enum Profile {
        PRIVATE, INSTITUTIONAL;
    }

    private Long id;

    private String name;

    private Profile profile;

    public Customer(Long id, String name, Profile profile) {
        this.id = id;
        this.name = name;
        this.profile = profile;
    }

    public Customer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
