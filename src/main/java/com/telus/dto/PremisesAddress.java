package com.telus.dto;

public class PremisesAddress {

    private String street;

    private int streetNumber;

    private String city;

    private String postCode;

    public PremisesAddress(String street, int streetNumber, String city, String postCode) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
        this.postCode = postCode;
    }

    public PremisesAddress() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

}
