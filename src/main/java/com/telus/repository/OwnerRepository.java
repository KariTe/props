package com.telus.repository;

import com.telus.model.OwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OwnerRepository extends JpaRepository<OwnerEntity, Long> {

    @Query("select o from com.telus.model.OwnerEntity o " +
            "left join fetch o.premises p " +
            "where o.id = :ownerId")
    OwnerEntity findOwnerByIdWithPremises(@Param("ownerId") Long ownerId);

}
