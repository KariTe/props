package com.telus.repository;

import com.telus.model.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PremisesAddressRepository extends JpaRepository<AddressEntity, Long> {
}
