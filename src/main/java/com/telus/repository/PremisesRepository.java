package com.telus.repository;

import com.telus.model.PremisesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface PremisesRepository extends JpaRepository <PremisesEntity, Long>  {

    @Modifying
    @Query("DELETE FROM PremisesEntity p WHERE p.id = ?1")
    void deleteOnePremises(Long premisesId);

    @Query("SELECT p FROM PremisesEntity p WHERE p.id = ?1")
    PremisesEntity getOnePremises(Long premisesId);

//    @Query("SELECT p FROM PremisesEntity p WHERE p.owner.id = ?1")
//    Set<PremisesEntity> findPremisesByOwnerId(Long ownerId);

}
