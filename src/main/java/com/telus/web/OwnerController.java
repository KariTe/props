package com.telus.web;

import com.telus.dto.Owner;
import com.telus.dto.Premises;
import com.telus.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RequestMapping("/owners")
@RestController
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value= "")
    public void create(@RequestBody (required = false) Owner ownerDto) {
        Long id = ownerService.create(ownerDto);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value = "/{id}")
    public Owner find(@PathVariable("id") Long id){
        return ownerService.find(id);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value = "/")
    public List<Owner> findAll(){
       return ownerService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/")
    public void update(@RequestBody Owner ownerDto) {
        ownerService.update(ownerDto);
    }

    @ResponseStatus(HttpStatus.GONE)
    @DeleteMapping(value = "/{id}")
    public void delete(Long id){
        ownerService.delete(id);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value= "/{ownerId}/premises")
    public Set<Premises> findPremises(@PathVariable ("ownerId") Long id) {
        return ownerService.findPremisesByOwnerId(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value= "/{ownerId}/premises")
    public void addPremises(@PathVariable ("ownerId") Long ownerId, @RequestBody Premises premisesDto) {
        ownerService.addPremises(ownerId,premisesDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value= "/{ownerId}/premises/{premisesId}")
    public void updatePremises(@PathVariable ("ownerId") Long ownerId, @PathVariable ("premisesId") Long premisesId, @RequestBody Premises premisesDto) {
        premisesDto.setId(premisesId);
        premisesDto.setOwnerId(ownerId);
        ownerService.updatePremises(ownerId, premisesId, premisesDto);
    }

    @DeleteMapping(value= "/{ownerId}/premises/{premisesId}")
    public void deleteOnePremises(@PathVariable ("ownerId") Long ownerId, @PathVariable ("premisesId") Long premisesId) {
        ownerService.deleteOnePremises(ownerId, premisesId);
    }

    @GetMapping(value= "/{ownerId}/premises/{premisesId}")
    public Premises getOnePremises(@PathVariable ("ownerId") Long ownerId, @PathVariable ("premisesId") Long premisesId) {
        return ownerService.getOnePremises(ownerId, premisesId);
    }

}
