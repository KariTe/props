package com.telus.web;

import com.telus.dto.User;
import com.telus.service.UserSrv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping ("/users")
@RestController
public class UserController {

    @Autowired
    private UserSrv userSrv;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/")
    public void create(@RequestBody (required=false) User userDto) {
        Long id = userSrv.create(userDto);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value = "/{id}")
    public User find(@PathVariable("id") Long id) {
        return userSrv.find(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/")
    public List<User> findAll() {
        return userSrv.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/")
    public void update(@RequestBody User userDto) {
        userSrv.update(userDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/{id}")
    public void delete(Long id) {
        userSrv.delete(id);
    }

}
