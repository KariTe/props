package com.telus.web;

import com.telus.dto.Premises;
import com.telus.service.PremisesSrv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/premises")
@RestController
public class PremisesController {

    @Autowired
    PremisesSrv premisesSrv;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/")
    public void create(@RequestBody Premises premisesDto){
        Long id = premisesSrv.create(premisesDto);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value = "/{id}")
    public Premises find(@PathVariable("id") Long id){
        return  premisesSrv.find(id);
    }

    @ResponseStatus(HttpStatus.FOUND)
    @GetMapping(value = "/")
    public List<Premises> findAll() {
        return premisesSrv.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/")
    public void update(@RequestBody Premises premises) {
         premisesSrv.update(premises);
    }

    @ResponseStatus(HttpStatus.GONE)
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        premisesSrv.delete(id);
    }

}
