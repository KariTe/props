package com.telus.service;

import com.telus.dto.Customer;
import com.telus.dto.User;
import com.telus.model.UserEntity;
import com.telus.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserSrvImpTest {

    @Autowired
    UserSrvImp userSrvImp;

    @Autowired
    UserRepository userRepository;

    Customer.Profile profile = Customer.Profile.INSTITUTIONAL;

    User.UserBusinessType type = User.UserBusinessType.OTHER;

//    @Transactional
    @Test
    public void shouldReturnExpectedObject() {

        // Given
        User userDto = new User((long) 1,"BlueMedia", profile, type);

        // When
        userSrvImp.create(userDto);

        // Then
        List<UserEntity> allUsers = userRepository.findAll();
        assertEquals(1, allUsers.size());
        assertEquals(userDto.getName(), allUsers.get(0).getName());
        assertEquals(userDto.getProfile().toString(), allUsers.get(0).getProfile().toString());
        assertEquals(userDto.getUserBusinessType().toString(), allUsers.get(0).getUserBusinessType().toString());

    }
}